var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var gulpsync = require('gulp-sync')(gulp);
var cssnano = require('gulp-cssnano');
var jade = require('gulp-jade');
var rename = require('gulp-rename');
var wrap = require('gulp-wrap');
var less = require('gulp-less');
var path = require('path');
var watch = require('gulp-watch');
var browserify = require('browserify');
var _ = require('lodash');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var utils = require('gulp-util');

gulp.task('build:clear', function () {
    return del(['src/public']);
});

var bundler = _.memoize(function () {
    var options = {debug: true};
    return browserify('./src/public-src/js/index.js', options);
});

var handleErrors = function () {
    var args = Array.prototype.slice.call(arguments);
    delete args[0].stream;
    utils.log.apply(null, args);
    this.emit('end');
};

function bundle(cb) {
    return bundler(watch)
    //.transform('jadeify', {extensions: ['jade']})
        .transform(require('jadeify'), {/*runtimePath: require.resolve('jade/runtime'),*/ extensions: ['jade']})
        .transform('babelify', {presets: ['es2015']})
        .bundle()
        .on('error', handleErrors)
        .pipe(source('index.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('src/public/js/'))
        .on('end', cb);
}

gulp.task('build:js', function (cb) {
    bundle(cb);
});

gulp.task('build:less', function () {
    return gulp.src('src/public-src/**/*.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest('src/public/'));
});

gulp.task('build', gulpsync.sync(['build:clear', ['build:js', 'build:less']]));

gulp.task('default', ['build']);

gulp.task('watch', ['build'], function () {
    watch(['src/**/*', '!./src/public/*', 'data/**/*'], function () {
        gulp.start('build');
    });
});