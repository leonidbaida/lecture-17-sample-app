var express = require('express');
var router = express.Router();
var Product = require('../mongo/Product');

router.get('/', function (req, res) {
    Product.find({}, function (err, products) {
        if (err) {
            console.error(err);
            return res.sendStatus(500);
        }
        res.json(products);
    });
});

module.exports = router;
