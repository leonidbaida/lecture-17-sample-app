import {LayoutView} from 'backbone.marionette';
import template from './template.jade';

export default LayoutView.extend({
    template,

    regions: {
        cart: '.cart-strip',
        products: '.products'
    }
});