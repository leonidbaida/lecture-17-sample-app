import {Router} from 'backbone';
import LayoutView from './LayoutView';
import CartStripView from './cart-strip/ItemView';
import CartService from '../cart/Service';
import ProductsView from './products/CollectionView';
import Products from './products/Collection';

export default Router.extend({
    initialize(options) {
        this.container = options.container;
    },

    routes: {
        products: 'index'
    },

    index() {
        var view = new LayoutView;
        this.container.show(view);

        var cartView = new CartStripView({
            collection: CartService.collection
        });

        var productsCollection = new Products();
        productsCollection.fetch();
        var productsView = new ProductsView({
            collection: productsCollection
        });

        view.cart.show(cartView);
        view.products.show(productsView);
    }
});