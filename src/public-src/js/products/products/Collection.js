import {Collection} from 'backbone';
import Model from './Model';

export default Collection.extend({
    url: '/products',
    model: Model
});