import {CompositeView} from 'backbone.marionette';
import template from './collection-template.jade';
import ItemView  from './ItemView';

export default CompositeView.extend({
    template,
    tagName: 'table',
    className: 'products',
    childView: ItemView,
    childViewContainer: 'tbody'
});