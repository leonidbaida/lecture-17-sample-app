import {ItemView} from 'backbone.marionette';
import template from './item-template.jade';

export default ItemView.extend({
    tagName: 'tr',
    className: 'product',
    template,

    initialize() {
        this.listenTo(this.model, 'change', this.render);
    },

    events: {
        'click button': 'toggle'
    },

    toggle() {
        this.model.toggle();
    }
});