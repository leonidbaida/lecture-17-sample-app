import {Model} from 'backbone';
import CartService from '../../cart/Service';

export default Model.extend({
    constructor(attributes, options) {
        attributes.added = CartService.has(attributes._id);
        return Model.call(this, attributes, options);
    },

    toggle() {
        CartService.toggle(this.get('_id'));
        this.set('added', CartService.has(this.get('_id')));
    }
});