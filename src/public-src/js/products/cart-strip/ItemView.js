import {ItemView} from 'backbone.marionette';
import template from './template.jade';

export default ItemView.extend({
    tagName: 'a',
    attributes: {
        href: '#cart'
    },

    initialize() {
        this.listenTo(this.collection, 'change add remove', this.render);
    },

    render() {
        this.$el.html(template({count: this.collection.length}));
        return this;
    }
});