import $ from 'jquery';
import {history} from 'backbone';
import Application from './application/Application';
import RootRouter from './root/Router';
import ProductsRouter from './products/Router';
import CartRouter from './cart/Router';
import OrderRouter from './order/Router';

$(function () {
    var app = new Application();

    new RootRouter;
    new ProductsRouter({
        container: app.layout.content
    });
    new CartRouter({
        container: app.layout.content
    });
    new OrderRouter({
        container: app.layout.content
    });

    app.on('start', () => history.start());
    app.start();
});