import {Router} from 'backbone';

export default Router.extend({
    routes: {
        '': 'root'
    },

    root() {
        this.navigate('products', {trigger: true});
    }
});