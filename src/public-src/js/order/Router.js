import {Router, Model} from 'backbone';
import {ItemView} from 'backbone.marionette';
import template from './template.jade';


var View = ItemView.extend({
    template
});

export default Router.extend({
    initialize(options) {
        this.container = options.container;
    },

    routes: {
        'order/:hash': 'order'
    },

    order(hash) {
        var model = new Model;
        model.fetch({url: '/order/' + hash}).done(() => this.container.show(new View({model: model}))
        );
    }
});
