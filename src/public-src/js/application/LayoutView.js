import {LayoutView} from 'backbone.marionette';
import template from './template.jade';

export default LayoutView.extend({
    el: 'body',
    template,

    regions: {
        header: '.application-header',
        content: '.application-content',
        footer: '.application-footer'
    }
});
