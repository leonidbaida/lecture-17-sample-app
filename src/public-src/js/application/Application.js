import {Application} from 'backbone.marionette';
import LayoutView from './LayoutView';
import $ from 'jquery';

export default Application.extend({
    initialize() {
        this.layout = new LayoutView;
        this.layout.render();
    }
});
