import {history} from 'backbone';
import {ItemView} from 'backbone.marionette';
import template from './template.jade';
import CartService from './Service';
import Model from './Model';

export default ItemView.extend({
    template,

    ui: {
        input: 'input'
    },

    events: {
        'click button[data-action=recalculate]': 'recalculate',
        'click button[data-action=order]': 'order'
    },

    initialize() {
        this.listenTo(this.model, 'change', this.render);
    },

    _updateCart() {
        this.ui.input.each((index, input) => {
            CartService.add(input.dataset.product, parseInt(input.value) || 0);
        });
    },

    recalculate() {
        this._updateCart();
        this.model.fetch({
            data: {
                cart: CartService.serialize()
            }
        })
    },

    order() {
        var email = window.prompt('Provide us with your email, please!');
        if (email) {
            var phone = window.prompt('Provide us with your phone number, please!');
            if (phone) {
                this._updateCart();
                new Model({
                    email: email,
                    phone: phone,
                    items: CartService.serialize()
                })
                    .save()
                    .done((data) => {
                        CartService.removeAll();
                        history.navigate('order/' + data.hash, {trigger: true, replace: true});
                    });
            }
        }
    }
});