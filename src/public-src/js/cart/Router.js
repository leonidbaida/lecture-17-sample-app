import {Router} from 'backbone';
import CartService from '../cart/Service';
import ItemView from './ItemView';
import Model from './Model';

export default Router.extend({
    initialize(options) {
        this.container = options.container;
    },

    routes: {
        cart: 'index'
    },

    index() {
        var model = new Model();
        model
            .fetch({
                data: {
                    cart: CartService.serialize()
                }
            })
            .done(() => {
                var view = new ItemView({
                    model: model
                });
                this.container.show(view);
            });
    }
});