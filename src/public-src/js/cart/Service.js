import {Model, Collection, LocalStorage} from 'backbone';
import Marionette from 'backbone.marionette';
import localstorage from 'backbone.localstorage';

var ProductModel = Model.extend({
    idAttribute: 'product'
});

var ProductCollection = Collection.extend({
    localStorage: new LocalStorage('cart'),
    model: ProductModel
});

var Service = Marionette.Object.extend({
    initialize() {
        this.collection = new ProductCollection();
        this.collection.fetch();
    },

    _find(id) {
        return this.collection.find(model => model.get('product') == id);
    },

    serialize() {
        return this.collection.toJSON();
    },

    add(id, quantity) {
        if (quantity === 0) {
            this.remove(id);

        } else {
            var model = this._find(id);
            if (model) {
                model.set('quantity', quantity || 1);
            } else {
                model = this.collection.add({
                    product: id,
                    quantity: quantity || 1
                });
            }
            model.save();
        }
    },

    remove(id) {
        var model = this._find(id);
        model.destroy();
    },

    toggle(id) {
        if (this.has(id)) {
            this.remove(id);
        } else {
            this.add(id);
        }
    },

    count() {
        return this.collection.length;
    },

    has(id) {
        return !!this._find(id);
    },

    removeAll() {
        while (this.collection.length > 0) {
            this.collection.first().destroy();
        }
    }
});

export default new Service;
